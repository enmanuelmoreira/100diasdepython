# 100 Días de Python - Día 3

## Control de Flujo y operadores Lógicos

### Condicional if/else

Dependiendo de una condición en particular, se ejecutará A o B, y cuando se quiera escribir codigo Python para representar esto, quedaria de la siguiente forma:

```python
if condicion:
    tarea
else:
    haz esta tarea
```

Ejemplo:

Realizar un programa en el que se admita la entrada a la montaña rusa a todas aquellas personas que midan mas de 120 cms de alto.

```python
print("Bienvenidos a la Montaña Rusa")
altura = int(input("Cual es su altura en cms?\n"))
if altura >= 120:
    print("Puedes Subir")
else:
    print("No puedes subir ya que no tienes la altura minima permitida")
```

| Operador  |   Significa   |
|    :-:    |       :-:     |  
|     >     | Mayor que     |
|     <     | Menor que     |
|     >=    | Mayor igual a |
|     <=    | Menor igual a |
|     ==    |    Igual a    |
|     !=    |  Diferente a[^nota1] |

[^nota1]: Se le conoce tambien como "no es igual a".

#### Ejercicio 1

Escriba un programa que valide que un numero dado sea par o impar.

El numero dado debe ser dividido entre 2 sin residuo.

Ejemplo: 86 es par porque 86 / 2 = 43

43 no contiene decimales, por lo que seria una division directa.

59 es impar, debido a que 59 / 2 = 36.875, que se puede observar que su division no es directa.

La función **modulo** esta referenciada por el signo de porcentaje (%) en Python. Lo que permite saber el restante después de calcular la división.

### Condicional if/else Anidado

En un condicional if/else anidado, una vez que la primera condicion se cumpla, se puede comprobar otra condicion en la que se tiene otra declaracion if/else.

```python
if condicion:
    if otra condicion:
        haz esto
    else:
        haz esto otro
else: 
    haz esta tarea
```

Siguiendo el ejemplo de la montaña rusa, se nos pide que si la edad del usuario es menor o igual a 18 años se le cobre la entrada a $7, si es mayor de 18 que la entrada cueste $12.

```python
print("Bienvenidos a la Montaña Rusa")
altura = int(input("Cual es su altura en cms?\n"))
edad = int(input("Cual es tu edad?\n"))
if altura >= 120:
    print("Puedes Subir")
    if edad <= 18:
        print("Debes pagar $12")
    else:
        print("Debes pagar $7")
else:
    print("No puedes subir ya que no tienes la altura minima permitida")
```

### Condicional if / elif / else

Si una condicion 1 no se cumple, podemos continuar comprobando la condicion 2, y si ninguna de estas fuera cierta, podemos ejecutar una condicion final (else)

```python
if condicion1:
    haz A
elif condicion2:
    haz B
else:
    haz esto
```

Siguiendo nuestro ejemplo de la montaña rusa, supongamos que ahora nos piden que a los menos de 12 años deben pagar 5, y los que estan entre 12 y 18 años deben pagar 7.

```python
print("Bienvenidos a la Montaña Rusa")
altura = int(input("Cual es su altura en cms?\n"))
edad = int(input("Cual es tu edad?\n"))
if altura >= 120:
    print("Puedes Subir")
    if edad < 12:
        print("Debes pagar $5")
    elif edad <= 18:
        print("Debes pagar $12")
    else:
        print("Debes pagar $7")
else:
    print("No puedes subir ya que no tienes la altura minima permitida")
```

#### Ejercicio 2

Haremos una actualización a nuestro programa que calculaba el Indice de Masa Corporal. Vamos a comprobar lo siguiente:

- Si el resultado esta por debajo de 18.5 entonces seria **Peso Inferior al Normal**
- Si el resultado esta sobre 18.5 pero por debajo de 25 **Peso Normal**
- Si el resultado esta sobre 25 pero es inferior a 30 **Sobrepeso**
- Si el resultado esta sobre 30 pero debajo de 35 **Obeso**
- Si el resultado esta sobre 35 **Clinicamente Obeso**

#### Ejercicio 3

Escriba un programa que valide si un año es bisiesto. Un año normal tiene 365 dias, los años bisiestos tiene 366 dias, con un dia extra en Febrero.

Cada año tiene que ser divisible entre 4.
Excepto que cada año es divisible entre 100.
A menos que ese año sea tambien divisible entre 400.

**Ejemplo:** El año 2000

- 2000 / 4 = 500 (bisiesto)
- 2000 / 100 = 20 (no bisiesto)
- 2000 / 400 = 5 (bisiesto)

Por lo que el año 2000 es bisiesto.

El año 2100 no seria bisiesto porque:

- 2100 / 4 = 525 (bisiesto)
- 2100 / 100 = 21 (no bisiesto)
- 2100 / 400 = 5.25 (no bisiesto)

### Multiples Condicionales if

Nos sirve para comprobar multiples condiciones aún cuando la anterior fuere correcta.

```python
if condicion1:
    haz A
if condicion2:
    haz B
if condicion3:
    haz C
```

#### Ejercicio 4

Volviendo al ejemplo a anterior de la montaña rusa, hay una promo en la que te sacan fotografias en el tren si pagas $3 extras, independientemente de la edad del usuario.

```python
print("Bienvenidos a la Montaña Rusa")

altura = int(input("Cual es su altura en cms?\n"))
cuenta = 0
if altura >= 120:
    print("Puedes Subir")
    edad = int(input("Cual es tu edad?\n"))
    if edad < 12:
        cuenta = 5
        print("Ticket para niños pagan $5")
    elif edad <= 18:
        cuenta = 7
        print("Tickets para adolescentes pagan $7")
    else:
        cuenta = 12
        print("Tickets para adultos pagan $12")
    foto = input("Desea que se le tomen fotos? S o N.\n")
    if foto == 'S' or foto == 's':
        cuenta += 3
    print(f"El total a pagar seria: {cuenta}")
else:
    print("No puedes subir ya que no tienes la altura minima permitida")
```

#### Ejercicio 5

Felicidades! has conseguido un trabajo en Python Pizza. Tu trabajo es desarrollar un sistema para los pedidos de la tienda.

Basado en las ordenes de los usuarios, los precios de las pizzas son:

- Pizza Pequeña: 15
- Pizza Mediana: 20
- Pizza Grande: 25
- Peperonni Extra en Pizza Pequeña: +2
- Peperonni Extra en Pizza Mediana o Grande: +3
- Extra Queso para cualquier tipo de pizza: +1

Input de Ejemplo:

- Tamanio: "L"
- agregar_pepperoni: = "Y"
- extra_queso: = "N"

Output de Ejemplo:

- Total compra: 28
  
### Operadores Lógicos

- Operador **and**: en este tipo de operador, ambas condiciones tienen que ser verdaderas para que se pueda cumplir, y al haber alguna que no se cumpla, la condicion será falsa.
- Operador **or**: en este tipo de operador, cualquiera de las condiciones puede ser verdadera para que se pueda cumplir, y falsa si todas las condiciones no se cumplen.
- Operador **not**: es la negación de una condicion, si la condicion es falsa, se convertirá en verdadera.y viceversa.

Siguiendo el ejemplo de la montaña rusa, digamos que la empresa decidiera que para que todos los que están teniendo una crisis de mediana edad, se les dará las entradas gratis. La crisis de la mediana edad suele presentarse entre los 45 y los 55 años.


```python
print("Bienvenidos a la Montaña Rusa")

altura = int(input("Cual es su altura en cms?\n"))
cuenta = 0
if altura >= 120:
    print("Puedes Subir")
    edad = int(input("Cual es tu edad?\n"))
    if edad < 12:
        cuenta = 5
        print("Ticket para niños pagan $5")
    elif edad <= 18:
        cuenta = 7
        print("Tickets para adolescentes pagan $7")
    elif edad >= 45 and  edad <= 55:
        print("Todo va a estar bien. Te regalamos un viaje gratis!") 
    else:
        cuenta = 12
        print("Tickets para adultos pagan $12")
    foto = input("Desea que se le tomen fotos? S o N.\n")
    if foto == 'S' or foto == 's':
        cuenta += 3
    print(f"El total a pagar seria: {cuenta}")
else:
    print("No puedes subir ya que no tienes la altura minima permitida")
```

### Ejercicio 6

Serás el encargado de desarrollar un programa que valida la compatibilidd entre dos personas. Vas a usar un metodo super cientifico recomendando por BuzzFeed.

Tienes que escribir tu nombre y apellido junto con el nombre y apellido de la otra persona y detectar las letras que hagan match con la frase **"True Love"**, por lo que la suma de ambas cosas te dará el porcentaje de compatibilidad con esa persona. Buena suerte!

La función **lower()** cambia todas las letras en una cadena a minusculas.

La funcion **count()** nos permite identificar cuantas veces se cuenta una letra en una cadena.

Para scores con menos de 10 o mas que 90, el mensaje deberia ser: "Tu score es x, irán juntos como mentos y coca cola."

Para scores entre 40 y 50 el mensaje deberia ser: "Tu score es y, se ven bien juntos!"

En cualquier otro caso, el mensaje solo debe mencionar el score: "Tu score es: z."

## Proyecto Final

Estarás creando un juego llamado la Isla del Tesoro. Tendrá un mensaje de bienvenida, el cual seguirá una serie de pasos, como ir a la derecha, a la izquierda, nadar, puertas de colores para al final llegar al tesoro.

- Primer paso: Derecha (morir) o Izquierda (seguir)
- Segundo paso: Nadar (morir) o Esperar (seguir)
- Tercer paso: Puerta Roja (morir), Azul (morir), Amarilla (seguir y ganar.)