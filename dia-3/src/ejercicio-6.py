# encoding: utf-8
print("Bienvenidos a la Calculadora del Amor")
nombre1 = input("Cual es tu nombre? \n")
nombre2 = input("Cual es su nombre? \n")

cadena_combinada = nombre1 + nombre2
cadena_minusculas = cadena_combinada.lower()

t = cadena_minusculas.count('t')
r = cadena_minusculas.count('r')
u = cadena_minusculas.count('u')
e = cadena_minusculas.count('e')

true = t + r + u + e

l = cadena_minusculas.count('l')
o = cadena_minusculas.count('o')
v = cadena_minusculas.count('v')
e = cadena_minusculas.count('e')

love = l + o + v + e

love_score = int(str(true) + str(love))

if love_score < 10 or love_score > 90:
    print(f"Tu score es {love_score}, irán juntos como mentos y coca cola.")
elif love_score >=40 and  love_score <= 50:
    print(f"Tu score es {love_score}, se ven bien juntos!")
else:
    print(f"Tu score es: {love_score}")