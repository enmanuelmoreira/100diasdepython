# encoding utf-8
print("Bienvenidos a Python Pizza")
tamanio = input("Que tamaño de pizza desea hoy: [P]equeña, [M]ediana, o [G]rande \n")
agregar_pepperoni = input("Desea agregar Pepperoni? S o N \n")
extra_queso = input("Desea agregar Queso Extra? S o N \n")
total = 0

if tamanio == 'P' or tamanio == 'p':
    total +=15
elif tamanio == 'M' or tamanio == 'm':
    total += 20
else:
    total += 25

if agregar_pepperoni == 'S' or agregar_pepperoni == 's':
    if tamanio == "P":
        total += 2
    else:
        total += 3

if extra_queso == 'S' or extra_queso == 's':
    total += 1

print(f"El valor total de su pedido es: ${total}. Gracias por su compra.")
        

