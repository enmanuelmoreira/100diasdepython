# encoding utf-8
print("Bienvenidos a la Montaña Rusa")

altura = int(input("Cual es su altura en cms?\n"))
cuenta = 0
if altura >= 120:
    print("Puedes Subir")
    edad = int(input("Cual es tu edad?\n"))
    if edad < 12:
        cuenta = 5
        print("Ticket para niños pagan $5")
    elif edad <= 18:
        cuenta = 7
        print("Tickets para adolescentes pagan $7")
    elif edad >= 45 and  edad <= 55:
        print("Todo va a estar bien. Te regalamos un viaje gratis!") 
    else:
        cuenta = 12
        print("Tickets para adultos pagan $12")
    foto = input("Desea que se le tomen fotos? S o N.\n")
    if foto == 'S' or foto == 's':
        cuenta += 3
    print(f"El total a pagar seria: {cuenta}")
else:
    print("No puedes subir ya que no tienes la altura minima permitida")
