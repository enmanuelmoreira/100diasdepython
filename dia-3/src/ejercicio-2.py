# encoding: utf-8
altura = float(input("ingrese su altura en m: "))
peso = float(input("ingrese su peso corporal en kg: "))

imc = round(peso / altura ** 2)
if imc < 18.5:
    print(f"Su Indice de Masa Corporal es: {imc}. Estás con un Peso Inferior al Normal")
elif imc < 25:
    print(f"Su Indice de Masa Corporal es: {imc}. Tienes un Peso Normal")
elif imc < 30:
    print(f"Su Indice de Masa Corporal es: {imc}. Estás con Sobrepeso")
elif imc < 35:
    print(f"Su Indice de Masa Corporal es: {imc}. Estás Obeso")
else:
    print(f"Su Indice de Masa Corporal es: {imc}. Estás Clinicamente Obeso")  
