# encoding: utf-8
print('''
____________________________________________________________________
/ \-----     ---------  -----------     -------------- ------    ----\
\_/__________________________________________________________________/
|~ ~~ ~~~ ~ ~ ~~~ ~ _____.----------._ ~~~  ~~~~ ~~   ~~  ~~~~~ ~~~~|
|  _   ~~ ~~ __,---'_       "         `. ~~~ _,--.  ~~~~ __,---.  ~~|
| | \___ ~~ /      ( )   "          "   `-.,' (') \~~ ~ (  / _\ \~~ |
|  \    \__/_   __(( _)_      (    "   "     (_\_) \___~ `-.___,'  ~|
|~~ \     (  )_(__)_|( ))  "   ))          "   |    "  \ ~~ ~~~ _ ~~|
|  ~ \__ (( _( (  ))  ) _)    ((     \\//    " |   "    \_____,' | ~|
|~~ ~   \  ( ))(_)(_)_)|  "    ))    //\\ " __,---._  "  "   "  /~~~|
|    ~~~ |(_ _)| | |   |   "  (   "      ,-'~~~ ~~~ `-.   ___  /~ ~ |
| ~~     |  |  |   |   _,--- ,--. _  "  (~~  ~~~~  ~~~ ) /___\ \~~ ~|
|  ~ ~~ /   |      _,----._,'`--'\.`-._  `._~~_~__~_,-'  |H__|  \ ~~|
|~~    / "     _,-' / `\ ,' / _'  \`.---.._          __        " \~ |
| ~~~ / /   .-' , / ' _,'_  -  _ '- _`._ `.`-._    _/- `--.   " " \~|
|  ~ / / _-- `---,~.-' __   --  _,---.  `-._   _,-'- / ` \ \_   " |~|
| ~ | | -- _    /~/  `-_- _  _,' '  \ \_`-._,-'  / --   \  - \_   / |
|~~ | \ -      /~~| "     ,-'_ /-  `_ ._`._`-...._____...._,--'  /~~|
| ~~\  \_ /   /~~/    ___  `---  ---  - - ' ,--.     ___        |~ ~|
|~   \      ,'~~|  " (o o)   "         " " |~~~ \_,-' ~ `.     ,'~~ |
| ~~ ~|__,-'~~~~~\    \"/      "  "   "    /~ ~~   O ~ ~~`-.__/~ ~~~|
|~~~ ~~~  ~~~~~~~~`.______________________/ ~~~    |   ~~~ ~~ ~ ~~~~|
|____~jrei~__~_______~~_~____~~_____~~___~_~~___~\_|_/ ~_____~___~__|
/ \----- ----- ------------  ------- ----- -------  --------  -------\
\_/__________________________________________________________________/
''')

print("Bienvenidos a la Isla del Tesoro.")
print("Tu misión es encontrar el Tesoro escondido.")

eleccion1 = input('Estás en una encrucijada, donde quieres ir? Escribe "derecha" o "izquierda".\n').lower()

if eleccion1 == "izquierda":
    eleccion2 = input('Has caído en un lago, hay una isla en el medio del lago, Escribe "esperar" por una barcaza o escribe "nadar" para ir más rápido\n').lower()
    if eleccion2 == "esperar":
        eleccion3 = input('Has llegado a la isla, desarmado, entras a una casa y en el vestibulo hay 3 puertas, una roja, una amarilla y la otra azul. Cual color escoges?\n').lower()
        if eleccion3 == "roja":
            print("Has entrado a una habitación en llamas. Has muerto :(")
        elif eleccion3 == "azul":
            print("Has entrado a una habitación llena de mounstros. Has muerto :(")
        elif eleccion3 == "amarilla":
            print("Encontraste el Tesoro, Has Ganado!!! :) ")
        else:
            print("La puerta que has escogido no existe. Has muerto :(")
    else:
        print("Has sido atacado por un Tiburón. Has muerto :(")    
else:
    print("Caiste en un hoyo sin fondo. Has muerto :(") 
    
