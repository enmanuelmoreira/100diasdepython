# 100 Días de Python - Día 2

## Tipos de Datos Principales en Python

- **Strings**: cadena de caracteres. Podemos desmembrar esas cadenas de caracteres y sacar cada caracter de manera individual, al cual se le llama **Subscript**:
  
```python
print("hello"[0])
```

Los Strings son tratados cuando se encierran entre comillas dobles ( "" ) aun cuando la cadena esté conformada por números:

```python
print("123-hello"[2])
```

- **Integer**: se conforman por números naturales (sin decimales) Para declararlo, solo se tiene que escribir el número. Si
  
```python
print(123 + 345)
```

Para mejorar la visualización de números grandes, se pueden usar underscores ( _ ) donde corresponde cada separador de miles y el interprete lo leerá como un número normal.

```python
print(123_456_789)
```

- **Float**: corresponden a los números enteros con decimales. Para ser declarado como float, deben escribirse los decimales.

```python
print(3.145635)
```

- **Boolean**: corresponden a los valores de Verdadero o Falso (True ; False)

## Type Error, Type Checking y Type Conversion

El siguiente ejemplo nos dará un Type Error:

```python
num_char = len(input("Cual es tu nombre?"))
print("Tu nombre tiene " + num_char + " caracteres.")
```

Estos tipos de errores suelen ser del tipo de datos utulizados, vemos que dentro de una variable num_char estamos utilizando la funcion len() para contar cuantos caracteres tiene el nombre que vamos a proporcionar en el input, el cual estamos tratando como un string en la salida del mismo.

Para saber que tipo de datos tiene una variable determinada:

```python
type(num_char)
```

Para convertir esta variable de tipo integer a string:

```python
new_num_char = str(num_char)
```

Si queremos refactorizar nuestro código, tendríamos este resultado:

```python
num_char = len(input("Cual es tu nombre? "))
new_num_char = str(num_char)
print("Tu nombre tiene " + num_char + " caracteres.")
```

Se pueden convertir diversos tipos de datos entre sí.

```python
print(70 + float("100.5"))
```

Como se puede ver arriba, si ejecutaramos ese pedacito de codigo, tendriamos como resultado 170.5, ya que a pesar que el valor de 100.5 esté encerrado entre "" (string) al pasarle el tipo de datos float el interprete lo leerá como una varibale de tipo float, pudiendo cumplir la operacion matematica.

```python
print(str(70)+ str(100))
```

En este caso nos imprimiría 70100, ya que a pesar que lo que está encerrado entre la funcion str() es un tipo de dato integer, automaticamente se convertirá en string y al concatenarla daria ese valor final.

### Ejercicio 1

Escriba un programa que añada dos digitos numericos, por ejemplo, la entrada va a ser 35, por lo que la saluda deberia ser `3 + 5 = 8`

## Operadores Matematicos en python

3 + 5 suma
7 - 3 resta
3 * 2 multiplicacion
6 / 3 division

2 ** 2 potenciacion, donde el numero de la izquierda es la base y el numero de la dereche seria el exponente.

En Python, las prioridades en las operaciones se orquetrn de la siguiente forma:

Parentesis -> Exponentes -> Multiplicacion -> Division -> Suma -> Resta.

Aunque en la vida real se va  a priorizar el calculo que esté mas a la izquierda.

Ejemplo:

```python
print(3 * 3 + 3 / 3 - 3)
```

En este caso el orden seria:
3 * 3 = 9, luego
3 / 3 = 1, luego
9 + 1 = 10 y finalmente
9 - 3 = 7

Si colocaramos parentesis a la misma operacion:

```python
print(3 * (3 + 3) / 3 - 3)
```

Tendriamos que, la primera operacion a realizarse seria la que se encuentra dentro de esos parentesis. Luego el resultado de ese valor se dividiria entre 3 y al final se restaria 3.

### Ejercicio 2

Escribir un programa que calcule el Indice de Masa Corporal del usuario, teniendo en cuenta como datos su altura y su peso.

El Indice de Masa Corporal es calculado dividiendo el peso de la persona (en kg) entre su altura al cuadrado.

## Manipulación de Números y F Strings en python

Hay veces en las que tenemos una variable de tio float en la cual, al convertirla a integer, aun cuando los decimales sean mayores a 5, este numero se redondea hacia abajo, por ejemplo: 4.666, se redondearia a 4.

Con la funcion `round()`, el numero se redondeará tomando en consideración los decimales.

```python
print(round(8 / 3), 2)
```

El valor despues de la coma, lo que va hacer es especificar el numero de digitos de precision a los que se quiere redondear.

En las divisiones, sobretodo en las inexactas, donde nos va a quedar como resultado un numero natural, podemos convertirla en decimal con la operacion F Divition.

```python
print(8 // 3)
```

Podemos "concatenar" operaciones matematicas:

```python
resultado = 4 / 2
resultado /= 2
print(resultado)
```

El ejemplo de arriba lo que va a hacer es dividir 4 entre 2, almacenarlo en la variable resultado, luego esa variable resultado se va a dividir entre 2 nuevamente dando como resultado 1.

Imaginemos un juego de basket donde el jugador anota:

```python
score = 0

# El usuario anota un punto:
score += 1 # toma el valor anterior de score y le suma 1
print(score)
```

F Strings permite que sea muy facil de mezclar strings y diferentes tipos de datos.

```python
score = 0
altura = 1.8
estaGanando = True
print("Tu Score es ", + str(score))
```

En vez de lo de arriba, con F Strings tendriamos lo siguiente:

```python
score = 0
altura = 1.8
estaGanando = True
print(f"Tu Score es {score}, tu altura es {altura}, gana {estaGanando}")
```

### Ejercicio 3

Crea un programa usando operadores matematicos y F Strings que nos diga cuantos dias, semanas y meses  nos quedarian de aqui a cumplir 90 años.

## Proyecto Final

Realizar un programa que permita calcular la propina en un restaurante. Debe preguntar lo siguiente:

- El total de la Cuenta.
- Que porcentaje de propina desea añadir (10, 12 o 15%)
- Con cuantas personas se dividirá la cuenta.
- El resultado total por persona.