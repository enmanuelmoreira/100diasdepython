# encoding: utf-8
cuenta = float(input("Cual es el total de la cuenta?: "))
propina = float(input("Que porcentaje agregar de propina: 10, 12 o 15?: "))
nro_personas = int(input("Con cuantas personas se va a dividir la cuenta?: "))

porcentaje_propina =  propina / 100
total_propina =  propina * porcentaje_propina
cuenta_con_propina = cuenta + total_propina
total_por_persona = cuenta_con_propina / nro_personas
monto_total = round(total_por_persona, 2)


mensaje = f"Cada persona debe pagar: {monto_total}" 
print(mensaje)