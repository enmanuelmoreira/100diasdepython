# 100 Días de Python - Día 1

## Funcion Print en Python

Se declara de la siguiente manera:

```python
print("cadena a imprimir")
```

```python
print("Funcion Print en Python")
print("TSe declara de la siguiente manera:")
print("print('cadena a imprimir')")
```

Para imprimir funciones como cadena se deben encerralas entre comillas simples ('') y afuerade la cadena entre comillas dobles (""), tambien puede ser a la inversa.

Para dar saltos de linea, utilizamos \n, no debe haber espacios entre la \n y la siguiente cadena.

```python
print("hello world\nhello world\nhello world")
```

## Concatenar cadenas de caracteres
Es simplemente tomar cadenas de caracteres separadas y unirlas en una sola.
Podemos combinar dos o mas cadenas de caracteres para que se agreguen al final de la otra cadena.

```python
print("Hello" + "Enmanuel")
```

Como se puede observar, ambas cadenas estan unidas, por lo que si queremos darle alguna separacion, hay dos formas de hacerlo:

- Agregar un espacio a una de las cadenas (al principio de una) o al final de la otra)
- O agregar un nuevo + para contanenar el espacio:

```python
print("Hello " + "Enmanuel")
print("Hello" + " " + "Enmanuel")
```

Los espacios en Python realmente importan, especialmente en el código. Python está diseñado para que se cumplan ciertas reglas en el indentado, normalmente son 2 espacios o un TAB, de esta manera, nos evitamos encerrar entre llaves como en otros lenguajes de programación los bloques de codigo if, for where, etc.

# Función Input

Nos permite ingresar datos para que puedan ser procesados o utilizados por nuestro codigo.

```python
input("¿Cual es tu nombre?")
```

```python
input("Hello " + input("¿Cual es tu nombre?")")
```

Para que una linea de codigo no sea considerada por Python, se debe anteponer un # al codigo para que sea omitido.

```python
# Este es un comentario
```

### Ejercicio 1

Comprobar cuantos caracteres tiene una cadena:

```python
print(len(input("Cual es tu nombre? ")))
```

## Variables

Son valores que son asignados en el codigo y puede ser utilizados por nuestro programa.

Se confoma por un nombre y su valor. En Python el tipo de valor se asigna automaticamete solo con declararlo con cualquier valor.

```python
nombre = input("¿Cual es tu nombre?")
print(nombre)
```

En caso de declarar dos variables con el mismo nombre, en el momento de ejecucion se va a sobreescribir el valor de ultima variable.

```python
nombre = "Jack"
print(nombre)

nombre = "Angela"
print(nombre)
```

Podemos tambien el tamaño de la cadena que está almacenada en la variable:

```python
nombre = input("Cual es tu nombre?")
lenght = len(name)
print(lenght)
```

### Ejercicio 2

Escriba un programa en el que se intercambien los valores almacenados en las variables a y b.

Ejemplo:
a = 5
b = 10

```python
a = input("a:")
b = input("b:")

c = a
a = b
b = c

print("a: " + a)
print("b: " + b)
```

Se pueden nombrar las variables como se desse, sin embargo, hay unas reglas que se deben cumplir para que el codigo sea lo mas legible:

- Deben tener relacion con el contenido que se va a almacenar.
- No deben contener espacios. Se puede utilizar el simbolo underscore ( _ ) para representar ese espacio.
- Los nombres de las variables no deben comenzar por un número.
- Las palabras reservadas como input, print, etc, no pueden ser usadas como variables.

## Proyecto Final

Realizar un programa en el se genere automaticamente el nombre de una banda. Debe realizar las siguientes tareas:

- Crear un saludo de bienvenida.
- Preguntar al usuario en que ciudad nació o creció.
- Preguntar al usuario el nombre de alguna mascota.
- Combinar el nombre de su ciudad y la de su mascota y mostrar el nombre de la banda.
