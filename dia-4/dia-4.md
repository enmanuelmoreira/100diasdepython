# 100 Días de Python - Día 4

## Aleatorización en Python

### Modulo Random

Nos facilita la generación de números aleatorios. Algunas de sus funciones mas importantes son las siguientes:

- **randint(a,b)**: Retorna un entero aleatorio entre a y b (ambos inclusive). Tambien retorna un ValueError si a > b.

```python
import random

numero_aleatorio =  random.randint(1, 100)
print(numero_aleatorio)
```

- **random()**: Retorna un numero entero con decimal entre 0.0 a 1.0.

```python
import random

entero_aleatorio = random.random()
print(entero_aleatorio)
```

#### Ejercicio 1

Debes escribir un programa donde sea lanzada una moneda virtual al aire diciendole al usuario que ha caido Cara o Cruz.

**Importante**: La primera letra debe estar en mayusculas. Por ejemplo "Cara" y no "cara"

### Listas

Son datos agrupados de los cuales se tengan relación. En Python una lista puede contener distintos tipos de datos como cadenas, numeros o booleanos encerrados entre corchetes [] y cada datos separado por una coma.

El orden en la lista está determinado por la lista en si, es decir el primer dato almacenado será el primero en mostrarse, luego el segundo dato y asi sucesivamente. Cabe destacar que el indice de la lista siempre comienza en 0.

```python
frutas = ["platano", "manzana"]
```

Para acceder a un elemento de la lista:

```python
regiones = ["I","II","III","IV","V"]

print(regiones[0])
```

Si en vez de consultar la lista con numeros positivos y lo hicieramos con negativos, en el caso de -1, estariamos recorriendo el ultimo elemento de la lista hacia atras, -2 el penúltimo y asi sucesivamente.

Para sustituir un valor de la lista, hacemos referencia a su indice y asignamos (cambiamos) su valor de la siguiente manera:

```Python
regiones = ["I","II","III","IV","V"]
regiones[4] = "VI" 
```

Lo que sustituirá el elemento 4 (V) por VI.

Para añadir elementos al final de la lista:

```python
regiones = ["I","II","III","IV","VI"]
regiones.append("VII")
```

Para extender o ampliar la lista con otra lista:

```python
regiones = ["I","II","III","IV","VI","VII"]
regiones.extend(["VIII","IX","X","XI")]
```

#### Ejercicio 2

Escriba un programa en el cual la persona que va a pagar el total de la cuenta sea al azar. Debe aparecer el nombre de la persona.

### IndexErrors y Listas Anidadas

Cuando trabajamos con listas es muy común que presente el error IndexError, y es cuando un elemento no existe en la lista, como por ejemplo, al tratar de llamar un elemento de la lista de regiones, donde teniamos 6 regiones (0 al 5) si intentaramos acceder al elemento 6 o 7, nos dará IndexError.

Supongamos que tenemos las siguientes listas:

```python
frutas = ["manzanas", "Uvas", "Duraznos", "Cerezas", "Peras"]

vegetales = ["espinacas","tomates","celery","papas"]
```

Para anidarlas, o que tengamos una lista y dentro las listas frutas y vegetales, hariamos lo siguiente:

```python
frutas = ["manzanas", "Uvas", "Duraznos", "Cerezas", "Peras"]

vegetales = ["espinacas","tomates","celery","papas"]

manjares = [frutas, vegetales]

print(manjares)

# imprimiria lo siguiente:

#[['manzanas', 'Uvas', 'Duraznos', 'Cerezas', 'Peras'], ['espinacas','"tomates','celery','papas']]
```

#### Ejercicio 3

Escriba un programa el cual marque con una X estilo tres en linea.

El mapa debe estar hecho de 3 filas de cuadrados en blanco o vacios.

El programa debe permitir marcar la posicion del  tesoro usando un sistema de dos digitos. El primer digito es la columna y el segundo digito la fila. Por ejemplo:

Columna 2, fila 3, debe ser tomada como: 23.

## Proyecto Final

Haz un programa donde podamos jugar el clasico juego: Piedra, Papel o Tijera.

La interfaz debe contar con opciones para poder escoger: Piedra

- 0 para Piedra
- 1 para Papel
- 2 para Tijera

El juego tiene 3 posibles finales: Ganar, Perder o Empate.

## Referencias

- <https://es.wikipedia.org/wiki/Mersenne_twister>
- <https://www.khanacademy.org/computing/computer-science/cryptography/crypt/v/random-vs-pseudorandom-number-generators>
- <https://www.askpython.com/python-modules/python-random-module-generate-random-numbers-sequences>
- <https://docs.python.org/es/3/tutorial/datastructures.html>
- <https://www.askpython.com/python/string/convert-string-to-list-in-python>
- <https://wrpsa.com/>