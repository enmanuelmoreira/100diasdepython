# encoding: utf-8
import random

nombres = input("Deme los nombres de todos, separados por una coma.\n")

lista_nombres = nombres.split(", ")

# Obtiene el numero total de items en la lista
num_items = len(lista_nombres)

# Genera numeros aleatorios entre el 0 y el ultimo indice de la lista
persona_aleatoria = random.randint(0, num_items - 1)
persona_quien_paga = lista_nombres[persona_aleatoria]

print("Quien paga la cuenta será: " + persona_quien_paga)