# encoding: utf-8

fila1 = ["⬜️", "⬜️", "⬜️"]
fila2 = ["⬜️", "⬜️", "⬜️"]
fila3 = ["⬜️", "⬜️", "⬜️"]

mapa = [fila1, fila2, fila3]

posicion = input("Donde quieres colocar tu tesoro? ")

horizontal = int(posicion[0])
vertical = int(posicion[1])

mapa[vertical - 1][horizontal - 1] = "X"

print(f"{fila1}\n{fila2}\n{fila3}")