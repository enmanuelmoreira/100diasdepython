# encoding: utf-8

import random

piedra = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

papel = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

tijeras = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

imagenes_juego = [piedra, papel, tijeras]

usuario = int(input("Que Eliges? Escribe 0 para Piedra, 1 para Papel o 2 para Tijeras.\n"))
print(imagenes_juego[usuario])

computadora = random.randint(0, 2)
print("La Computadora elige:")
print(imagenes_juego[computadora])

if usuario >= 3 or usuario < 0: 
  print("Has digitado un número incorrecto, Perdiste!") 
elif usuario == 0 and computadora == 2:
  print("Ganaste!")
elif computadora == 0 and usuario == 2:
  print("Perdiste!")
elif computadora > usuario:
  print("Perdiste!")
elif usuario > computadora:
  print("Ganaste!")
elif computadora == usuario:
  print("Es un Empate!")
